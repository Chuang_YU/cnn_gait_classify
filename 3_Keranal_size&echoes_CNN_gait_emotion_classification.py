# -*- coding: utf-8 -*-
"""
e-mail: alexchauncy@gmail.com
2018-07
@author: Chuang YU ENSTA_ParisTech  
"""
#acc 42%--49% 

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
np.random.seed(1337) 
from keras.utils import np_utils
from keras.models import Sequential
from keras.layers import Dense, Activation, Convolution2D, MaxPooling2D, Flatten
from keras.optimizers import Adam

## load the database_chuang
gaitdata_1_DataFrame=pd.read_csv("E:\ensta_research\emotion_gait_and_thermal_facial_image\kinect_data\skeleton_data\gait_data_base_2400_56_after_medfilt.csv")
gaitdata_2=gaitdata_1_DataFrame.values[:,:]
gaitdata_3=gaitdata_2.reshape(300,1,8,56)
X=gaitdata_3
y=np.array([0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3,0,0,0,0,0,1,1,1,1,1,2,2,2,2,2,3,3,3,3,3])

# data pre-processing.............................................................
feature_maxs=[66.6875768653487,28.00530179872793,98.1741941489158,20.2249244064732,212.51638189202203,14.460468972020012,215.063469854803,31.954409878614]
feature_mins=[0.29858782013189,-19.969585800811398,0.22346874037266198,-22.814995502244702,131.05306266479099,-14.23406094317599,131.345011105322,-33.45968473706398]

def norm_func (x0,min0,max0):
    y0=(x0-min0)/(max0-min0)
    return y0
#X normalization
for ii in range(300):
    for jj in range(8):
        X[ii,0,jj,:]=norm_func(X[ii,0,jj,:],feature_mins[jj],feature_maxs[jj])
##divide into train set and test set
X_train,X_test,y_train,y_test=train_test_split(X, y, train_size = 200, random_state=1, stratify=y) 
# random_state取整数，表示每次结果不相同 shuttle=true 代表先打乱顺序 
#stratify=y就是输入的所有的y的集合，代表分成输入数据2:2:2 随机划分出来的test数据 也是1:1:1
#train_size = 200 也可以

#one-hot
y_train = np_utils.to_categorical(y_train, num_classes=4) # to_categorical one-hot transfer 0,1,2 into 1 0 0, 0 1 0, 0 0 1
y_test = np_utils.to_categorical(y_test, num_classes=4)

# Build your CNN
model = Sequential()

# Conv layer 1 output shape (60, 8, 54)
model.add(Convolution2D(
    batch_input_shape=(200, 1, 8, 56),
    filters=60,
    kernel_size=(1,3),#list/tuple
    strides=1,
    padding='same',    # Padding method
    data_format='channels_first' #（3,128,128） or channels_last (128,128,3)
))
model.add(Activation('relu'))
## Pooling layer 1 (max pooling) output (60,8,27)
model.add(MaxPooling2D(
    pool_size=(1,2),
    strides=(1,2),
    padding='same',    #Padding method
    data_format='channels_first',
))


# Conv layer 2 output shape (60, 8, 25)
model.add(Convolution2D(60, [1,3], strides=1, padding='same', data_format='channels_first'))
model.add(Activation('relu'))
# Pooling layer 2 (max pooling) output shape (60, 8, 13)
model.add(MaxPooling2D((1,2), (1,2), 'same', data_format='channels_first'))

# Conv layer 3 output shape (40, 8, 9)
model.add(Convolution2D(40, [1,3], strides=1, padding='same', data_format='channels_first'))
model.add(Activation('relu'))
# Pooling layer 3 (max pooling) output shape (40, 8, 6)
model.add(MaxPooling2D((1,2), (1,2), 'same', data_format='channels_first'))




# Fully connected layer 1 input shape (40 * 8 * 4) = (1280), output shape (500)
model.add(Flatten())
model.add(Dense(500))
model.add(Activation('relu'))

# Fully connected layer 3 to shape (4) for 4 classes
model.add(Dense(4))
model.add(Activation('softmax'))

# Another way to define your optimizer
adam = Adam(lr=1e-4) #lr=learning rate

# We add metrics to get more results you want to see. metrics:指标
model.compile(optimizer=adam,
              loss='categorical_crossentropy', #binary_crossentropy（亦称作对数损失，logloss）
              metrics=['accuracy'])

print('Training ------------')
# Another way to train the model
model.fit(X_train, y_train, epochs=300, batch_size=300) #batch_size：整数，指定进行梯度下降时每个batch包含的样本数，可以采用full batch learning 数据量小的话

print('\nTesting ------------')
# Evaluate the model with the metrics we defined earlier
loss, accuracy = model.evaluate(X_test, y_test)
print('\ntest loss: ', loss)
print('\ntest accuracy: ', accuracy)
