# -*- coding: utf-8 -*-
"""
Created on Sun Jul 15 23:52:25 2018
@author: Chuang YU in ENSTA-ParisTech  alexchauncy@gmail.com
"""

#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
##### input:  X_Train= <class 'numpy.ndarray'>  ndarray is The N-dimensional array
##### output: Y_train=<class 'numpy.ndarray'> like [5 0 4 ... 5 6 8]
##### y_train = np_utils.to_categorical(y_train, num_classes=10)  
# =============================================================================
# [[0. 0. 0. ... 0. 0. 0.]
#  [1. 0. 0. ... 0. 0. 0.]
#  [0. 0. 0. ... 0. 0. 0.]
#  ...
#  [0. 0. 0. ... 0. 0. 0.]
#  [0. 0. 0. ... 0. 0. 0.]
#  [0. 0. 0. ... 0. 1. 0.]]
# <class 'numpy.ndarray'>
# =============================================================================
#### from "DataFrame" to "ndarray" 
# =============================================================================
# (1)DataFrame1.values
# (2)array(DataFrame1)
# =============================================================================
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

import pandas as pd
# name.............................................. without'pierre',
names=['ahang', 'arturo', 'chen', 'david', 'dulgheriu', 'moad', 'pessaux', 'roxana', 'rupu', 'sibo', 'thibault', 'wen', 'xian', 'xinyu', 'ziyi']
#emotion order########################################
emotion_orders=["11n","23n","34n","42n","51n","12h","21h","33h","44h","52h","13a","24a","32a","41a","53a","14s","22s","31s","43s","54s"]

#save the 320 sequence length in CSV
save_size_file = open(r'E:\ensta_research\emotion_gait_and_thermal_facial_image\kinect_data\skeleton_data\sequence_lenght_320.csv', "w")
# save the 320 sequence length in List
sequence_size_list=[]

for name in names:
    for emotion_order in emotion_orders:
        # read the gait data as Data_Frame
        gait_data_8=pd.read_table('E:/ensta_research/emotion_gait_and_thermal_facial_image/kinect_data/skeleton_data/'+name+'/'+name+"-"+emotion_order+'/Skeleton/gait_data.txt',delim_whitespace=True)
        #-1: first row is 0 which is unuseful (Rows and Columns)
        size1=gait_data_8.values[:,0].size-1 
        print(size1) 
        
        # save the 320 sequence length in List
        sequence_size_list.append(size1)
        #save the 320 sequence length in CSV
        save_size_file.write(str(size1)+"\n")  
#everytime you should close,otherwise, the data recording will need to Run 2 times.
save_size_file.close()  
print(sequence_size_list)
print(min(sequence_size_list))



  
  


