# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
# name.............................................. without'pierre',
names=['ahang', 'arturo', 'chen', 'david', 'dulgheriu', 'moad', 'pessaux', 'roxana', 'rupu', 'sibo', 'thibault', 'wen', 'xian', 'xinyu', 'ziyi']
#emotion order########################################
emotion_orders=["11n","23n","34n","42n","51n","12h","21h","33h","44h","52h","13a","24a","32a","41a","53a","14s","22s","31s","43s","54s"]
#save the 300 sequence length in CSV
save_size_file = open(r'E:\ensta_research\emotion_gait_and_thermal_facial_image\kinect_data\skeleton_data\sequence_lenght_320.csv', "w")
# save the 300 experiments  sequence length in List

# for first time save gait data
Onetime_flag=True

for name in names:
    for emotion_order in emotion_orders:
        # read the gait data as Data_Frame
        gait_data_8=pd.read_table('E:/ensta_research/emotion_gait_and_thermal_facial_image/kinect_data/skeleton_data/'+name+'/'+name+"-"+emotion_order+'/Skeleton/gait_data.txt',delim_whitespace=True)
        #gait_data_8: type is DataFrame #-1: first row is 0 which is unuseful (Rows and Columns)
        gait_X_all_ndarray_oneimage=gait_data_8.values[2:58,12:20]
        #2:58 means 2,3,4,5.....57  #one image means 8 sequences*56 data of every sequence= similar with one image in CNN
        #print("type(gait_data_8.values):",type(gait_data_8.values))#gait_data_8.values：type is " <class 'numpy.ndarray'> "
        for a_chuang in [1,3,5,7]: # for velocity
           gait_X_all_ndarray_oneimage[:,a_chuang]= gait_X_all_ndarray_oneimage[:,a_chuang]/30# the velocity become the angle difference. 1 second 30 frame.
        
        #Transpose
        gait_X_all_ndarray_oneimage = np.transpose(gait_X_all_ndarray_oneimage)
        
        #reshape [8,56] to[1,8,56]
        gait_X_all_ndarray_oneimage=gait_X_all_ndarray_oneimage.reshape(1,8,56)
        
        if Onetime_flag==True:
           Gait_X_all_ndarray=gait_X_all_ndarray_oneimage
           Onetime_flag=False 
        else:
           Gait_X_all_ndarray=np.vstack([Gait_X_all_ndarray,gait_X_all_ndarray_oneimage])
           print(type(gait_X_all_ndarray_oneimage))    
#reshape 
Gait_X_all_ndarray=Gait_X_all_ndarray.reshape(300,1,8,56)  
print("Gait_X_all_ndarray[0,0,:,:]:",Gait_X_all_ndarray[0,0,:,:])
print("Gait_X_all_ndarray type: ",type(Gait_X_all_ndarray))
print("Gait_X_all_ndarray size:",Gait_X_all_ndarray.size)  
print("Gait_X_all_ndarray shape:",Gait_X_all_ndarray.shape)

# =============================================================================
# ###### Gait_X_all_ndarray: saveing into the  csv
gaitdata_0=Gait_X_all_ndarray.reshape(2400,56)
gaitdata_DataFrame=pd.DataFrame(gaitdata_0)
gaitdata_DataFrame.to_csv("E:\ensta_research\emotion_gait_and_thermal_facial_image\kinect_data\skeleton_data\gait_data_base_300_8_56.csv",index=False)

# #### Test it
gaitdata_1_DataFrame=pd.read_csv("E:\ensta_research\emotion_gait_and_thermal_facial_image\kinect_data\skeleton_data\gait_data_base_300_8_56.csv")
gaitdata_2=gaitdata_1_DataFrame.values[:,:]
gaitdata_3=gaitdata_2.reshape(300,1,8,56)
print(gaitdata_3.size,";",type(gaitdata_3),";",gaitdata_3.shape,";")

difference=gaitdata_3-Gait_X_all_ndarray
print(difference)
#print("Gait_X_all_ndarray[0,0,:,:]:",Gait_X_all_ndarray[0,0,:,:])
# =============================================================================

